/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Negocio;

import Modelo.Etiqueta;
import Modelo.PaginaWeb;
import Util.ArchivoLeerURL;
import Util.ColaModified;
import Util.ListaCD;

/**
 *
 * @author Adolfo Alejandro Arenas Ramos
 */
public class EvaluadorHTML {

    private ListaCD<Etiqueta> etiquetas[] = new ListaCD[2];
    private PaginaWeb myPagina = new PaginaWeb();

    public EvaluadorHTML() {
        this.etiquetas[0] = new ListaCD();
        this.etiquetas[1] = new ListaCD();
    }

    public void cargarPaginaWeb(String url) {
        myPagina.cargarPagina(url);
    }

    //<img>
    //<input>
    //<br>
    public void cargarEtiquetas(String url) {
        ArchivoLeerURL f = new ArchivoLeerURL(url);
        Object[] x = f.leerArchivo();
        for (int i = 1; i < x.length; i++) {
            String fila = x[i].toString();
            String[] info = fila.split(",");
            Etiqueta etq = new Etiqueta(info[0]);
            if ("false".equals(info[1])) {
                this.etiquetas[0].insertarFin(etq);
                continue;
            }
            this.etiquetas[1].insertarFin(etq);
        }
    }

    private boolean validarEtiquetas() {
        ColaModified<Etiqueta> colaEtiquetas = new ColaModified<>();
        for (int i = 0; i < etiquetas[1].getSize(); i++) {
            colaEtiquetas.enColar(etiquetas[1].get(i));
        }
        return false;
    }
    
    public String toString_PaginaWeb() {
        return myPagina.toString();
    }

    public String toString_Etiquetas() {
        
        StringBuilder nonBinary = new StringBuilder();
        nonBinary.append("\n*** Etiquetas no Binarias ***\n");
        for (int i = 0; i < this.etiquetas[0].getSize(); i++) {
            nonBinary.append("\n").append(this.etiquetas[0].get(i)).append(" -> no es una binaria");
        }
        
        StringBuilder yesBinary = new StringBuilder();
        yesBinary.append("\n*** Etiquetas Binarias ***\n");
        for (int i = 0; i < this.etiquetas[1].getSize(); i++) {
            yesBinary.append("\n").append(this.etiquetas[1].get(i)).append(" -> es una binaria");
        }
        return nonBinary.toString() + yesBinary.toString();
    }

    public String validadorHTML() {
        return ":)";
    }

    public String getAutoEvaluacion() {
        return "La nota de mi autoevaluación la estipulo en un: 8";
    }
}