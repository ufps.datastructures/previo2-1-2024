/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Util.ArchivoLeerURL;

/**
 *
 * @author madar
 */
public class TestPruebaArchivoURL {
    public static void main(String[] args) {
        String urlEtiquetas="https://gitlab.com/pruebas_madarme/persistencia/editorhtml/-/raw/main/etiquetas_html_sin.csv";
        ArchivoLeerURL f=new ArchivoLeerURL(urlEtiquetas);
        Object x[]=f.leerArchivo();
        imprimir(x);
        String urlWeb="https://gitlab.com/pruebas_madarme/persistencia/editorhtml/-/raw/main/htmlBien.html";
        f=new ArchivoLeerURL(urlWeb);
        x=f.leerArchivo();
        imprimir(x);
        String urlWeb2="https://gitlab.com/pruebas_madarme/persistencia/editorhtml/-/raw/main/htmlMal.txt";
        f=new ArchivoLeerURL(urlWeb2);
        x=f.leerArchivo();
        imprimir(x);
    }
    
    private static void imprimir(Object x[])
    {
        System.out.println("------------------------------------------------------\n");
        for(Object fila:x)
        {
            System.out.println(fila.toString());
        }
    }
    
}
