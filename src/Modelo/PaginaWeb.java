/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

import Util.ArchivoLeerURL;
import Util.ListaCD;

/**
 *
 * @author Adolfo Alejandro Arenas Ramos
 */
public class PaginaWeb {

    private ListaCD<String> lineas;

    public PaginaWeb() {
        this.lineas = new ListaCD<>();
    }

    public void cargarPagina(String url) {
        ArchivoLeerURL f = new ArchivoLeerURL(url);
        Object[] x = f.leerArchivo();
        for (Object y : x) {
            this.lineas.insertarFin((String) y);
        }
    }

    @Override
    public String toString() {
        StringBuilder bd = new StringBuilder();
        for (int i = 0; i < this.lineas.getSize(); i++) {
            bd.append("\n").append(lineas.get(i));
        }
        return bd.toString();
    }
}